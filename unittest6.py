# -*- coding: utf-8 -*-
from collections import defaultdict
import unittest
import task6
class TestMethods(unittest.TestCase):
    def test_graph(self):
        w =[[0, 3, 4, -1, 1],
            [3, 0, 5, -1, -1],
            [4, 5, 0, 2, 6],
            [-1, -1, 2, 0, 7],
            [1, -1, 6, 7, 0]]    
        G = defaultdict(list)
        G[0].append(1)       
        G[0].append(2)        
        G[0].append(4)        

        G[1].append(0)        
        G[1].append(2)        

        G[2].append(0)        
        G[2].append(1)        
        G[2].append(3)        

        G[3].append(2)   
        G[3].append(4)   
 
        G[4].append(0)   
        G[4].append(2)   
        G[4].append(3) 
        self.assertEqual(task6.prim(G,w),{0: 0, 1: 3, 2: 5, 3: 7, 4: 1})

if __name__ == '__main__':
    unittest.main()