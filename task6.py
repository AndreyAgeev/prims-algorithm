from collections import defaultdict, deque


class Queue():
    def __init__(self, G, cost):
        self.H = [cost[i] for i in range(max(G)+1)]
        self.size = max(G)+1

    def extract_min(self):
        res = min(self.H)
        for i in range(0, len(self.H)):
            if(self.H[i] == res):
                self.H[i] = float("inf")
                self.size -= 1
                return i

    def change_priority(self, i, p):
        self.H[i] = p


def prim(G, w):
    cost = defaultdict(list)
    prev = defaultdict(list)
    for v in G:
        cost[v] = float("inf")
        prev[v] = None
    vertex = next(iter(deque(G)))
    cost[vertex] = 0
    H = Queue(G, cost)
    while H.size > 0:
        v = H.extract_min()
        for z in G[v]:
            if cost[v] < w[v][z]:
                cost[z] = w[v][z]
                prev[v] = v
                H.change_priority(z, cost[z])
    return cost
